import pandas as pd
import plotly.plotly as py
import plotly.tools as tls
import os
import re
import numpy as np
import nltk
#nltk.download() # After running this kernel you need to close the NLTK downloader window which pops up to further progress with the code
from nltk.tokenize import word_tokenize
import html
import matplotlib.pyplot as plt
from html.parser import HTMLParser

# Data Visualization libraries
import matplotlib.pyplot as plt
def sanitize_airline_tweets(airline_tweet):
    new_text=html.unescape(airline_tweet)   # converts HTML character codes to ASCII equivalent
    lower_case = new_text.lower()           # converts all the letters to lower case
    spaces = re.sub('[\s]+', ' ', lower_case) # Removes the spaces
    link = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','',spaces) # Removes the URL's

    alpha_numeric = re.sub('[^a-zA-Z\s]+', '', link) # Removes the alpha numeric symbols

    with open('stop_words.txt', 'r') as stop_words_file: # Used the given stopwords list in the assignment
        stop_words = stop_words_file.readlines()
    stop_words_list = list(map(lambda x:x.strip(),stop_words))
    word_tokens = word_tokenize(alpha_numeric)   # Tokenizes the text
    filtered_sentence = []
    for word  in word_tokens:
        if word  not in stop_words_list:
            filtered_sentence.append(word)



    return filtered_sentence





def tweets_airline(tweet):
    for i in airlines:
        if i in tweet:
            return i # Returns airline to every tweet in the data set

def visualize_distribution_of_sentiments():
    # Data to plot
    labels = ['Positive','Negative']
    sizes = generic_df['class'].value_counts()
    sizes = [504, 337, 415, 280]
    labels_gender = ['Man', 'Woman', 'Man', 'Woman', 'Man', 'Woman', 'Man', 'Woman']

    sizes_gender = [315, 189, 125, 212, 270, 145, 190, 90]
    colors = ['#ff6666', '#ffcc99', '#99ff99', '#66b3ff']
    colors_gender = ['#c2c2f0', '#ffb3e6', '#c2c2f0', '#ffb3e6', '#c2c2f0', '#ffb3e6', '#c2c2f0', '#ffb3e6']

    explode = (0.2, 0.2, 0.2, 0.2)
    explode_gender = (0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1)

    # Plot
    plt.pie(sizes, labels=labels, colors=colors, startangle=90, frame=True, explode=explode, radius=3)

    plt.pie(sizes_gender, colors=colors_gender, startangle=90, explode=explode_gender, radius=2)

    # Draw circle
    centre_circle = plt.Circle((0, 0), 1.5, color='black', fc='white', linewidth=0)
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)

    plt.axis('equal')
    plt.tight_layout()
    plt.show()


def visualize_words_distribution(airlines_df):
    y = airlines_df['airlines'].value_counts()
    print(y.plot.bar())
    plt.title("Distribution of the US airlines of the tweet")
    plt.xlabel('Airlines')
    plt.ylabel('No of tweets')
    plt.show()
if __name__ == '__main__':
    airlines_df = pd.read_csv('US_airline_tweets.csv')
    airlines_df = airlines_df.drop(['id', 'user', 'retweet_count'], axis=1)
    #parses over each row of the column 'text' in airline and sanitizes the tweets.
    airlines_df['cleaned_tweets'] = airlines_df['text'].apply(lambda airline_tweet: sanitize_airline_tweets(airline_tweet))
    airlines_df.head()
    airlines = ['virginamerica', 'united', 'southwestair', 'jetblue', 'usairways', 'americanair']
    airlines_df['airlines'] =airlines_df['cleaned_tweets'].apply(lambda airline_tweet: tweets_airline(airline_tweet))
    date = plt.figure()
    x = [airlines_df.head()]
    visualize_words_distribution(airlines_df)

   # ax = plt.subplot(111)
    #ax.bar([10,2],y[1], width=10)
    #plotly_fig = tls.mpl_to_plotly(ax)



